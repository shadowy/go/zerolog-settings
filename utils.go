package zerologsettings

func isNullString(value *string, def string) *string {
	if value != nil {
		return value
	}

	return &def
}

func isNullInt64(value *int64, def int64) *int64 {
	if value != nil {
		return value
	}

	return &def
}

func getPtrBool(value bool) *bool {
	return &value
}

var fieldsForUpdate = map[string]string{
	"time":        "@timestamp",
	"message":     "Message",
	"level":       "Level",
	"application": "Host",
}

func updateFields(data map[string]interface{}) map[string]interface{} {
	for key, value := range data {
		if field, ok := fieldsForUpdate[key]; ok {
			data[field] = value
			delete(data, key)
		}
	}

	return data
}
