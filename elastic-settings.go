package zerologsettings

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/olivere/elastic/v7"
	"github.com/rs/zerolog/log"
	"io"
	ol "log"
	"sync"
	"time"
)

type ElasticSettings struct {
	URL               string  `yaml:"url"`
	Host              string  `yaml:"host"`
	User              string  `yaml:"user"`
	Password          string  `yaml:"password"`
	Level             *string `yaml:"level"`
	FlushInterval     *int64  `yaml:"pushTime"`
	IndexName         *string `yaml:"indexName"`
	IndexSuffixFormat *string `yaml:"indexSuffixFormat"`

	processor       *elastic.BulkProcessor
	flushSync       sync.RWMutex
	flushProcessing bool
	flushRepeat     bool
}

func (settings *ElasticSettings) Init() error {
	settings.Level = isNullString(settings.Level, "info")
	settings.FlushInterval = isNullInt64(settings.FlushInterval, 10)
	settings.IndexName = isNullString(settings.IndexName, "log")
	settings.IndexSuffixFormat = isNullString(settings.IndexSuffixFormat, "2006-01-02")
	l := log.Logger.With().Str("url", settings.URL).Str("index", *settings.IndexName).Logger()

	var cfg []elastic.ClientOptionFunc
	cfg = append(cfg, elastic.SetURL(settings.URL))

	if settings.User != "" || settings.Password != "" {
		l.Info().Msg("ElasticSettings.Init use basicAuth")
		cfg = append(cfg, elastic.SetBasicAuth(settings.User, settings.Password))
	}

	client, err := elastic.NewSimpleClient(cfg...)
	if err != nil {
		l.Error().Err(err).Stack().Msg("ElasticSettings.Init NewClient")
		return err
	}

	settings.processor, err = client.BulkProcessor().
		Name("").
		BulkActions(-1).
		BulkSize(-1).
		FlushInterval(time.Millisecond * time.Duration(*settings.FlushInterval)).
		Do(context.Background())
	if err != nil {
		l.Error().Err(err).Stack().Msg("ElasticSettings.Init BulkProcessor")
		return err
	}

	return nil
}

func (settings *ElasticSettings) getWriter() (io.Writer, error) {
	if settings.processor == nil {
		log.Logger.Error().Stack().Msg("ElasticSettings.getWriter processor for elastic not defined")
		return nil, errors.New("client for processor not defined")
	}

	return settings, nil
}

func (settings *ElasticSettings) Write(p []byte) (int, error) {
	var data map[string]interface{}
	err := json.Unmarshal(p, &data)
	if err != nil {
		ol.Println("write err:" + err.Error())
		return 0, err
	}
	data = updateFields(data)
	index := fmt.Sprintf("%s-%s", *settings.IndexName, time.Now().Format(*settings.IndexSuffixFormat))
	r := elastic.NewBulkIndexRequest().Index(index).Type("log").Doc(data)
	settings.processor.Add(r)
	settings.flush()

	return len(p), nil
}

func (settings *ElasticSettings) flush() {
	if settings.processor == nil {
		return
	}
	settings.flushSync.RLock()
	if settings.flushProcessing {
		settings.flushSync.RUnlock()
		settings.flushSync.Lock()
		settings.flushRepeat = true
		settings.flushSync.Unlock()

		return
	}

	settings.flushSync.RUnlock()
	settings.flushSync.Lock()
	settings.flushProcessing = true
	settings.flushSync.Unlock()

	go func() {
		err := settings.processor.Flush()
		if err != nil {
			ol.Println("flush routing err:" + err.Error())
		}
		settings.flushSync.Lock()
		repeat := settings.flushRepeat
		settings.flushRepeat = false
		settings.flushProcessing = false
		settings.flushSync.Unlock()
		if repeat {
			settings.flush()
		}
	}()
}
