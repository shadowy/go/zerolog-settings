package zerologsettings

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"os"
	"time"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano})
}

// Settings - settings for logging.
type Settings struct {
	Application  *string          `yaml:"application"`
	File         *FileSettings    `yaml:"file"`
	Elastic      *ElasticSettings `yaml:"elastic"`
	GCOL         *GCOLSettings    `yaml:"gcol"`
	BlockConsole *bool            `yaml:"blockConsole"`
}

// Init - init settings.
func (settings *Settings) Init() error {
	if settings.BlockConsole == nil {
		settings.BlockConsole = getPtrBool(false)
	}
	var writers []io.Writer
	if !*settings.BlockConsole {
		writers = append(writers, zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339Nano})
	}
	if settings.File != nil {
		w, err := settings.File.getWriter()
		if err != nil {
			return err
		}
		writers = append(writers, w)
	}
	if settings.GCOL != nil {
		appID := ""
		if settings.Application != nil {
			appID = *settings.Application
		}
		w, err := settings.GCOL.getWriter(appID)
		if err != nil {
			return err
		}
		writers = append(writers, w)
	}
	if settings.Elastic != nil {
		err := settings.Elastic.Init()
		if err != nil {
			return err
		}
		w, err := settings.Elastic.getWriter()
		if err != nil {
			return err
		}
		writers = append(writers, w)
	}
	mw := io.MultiWriter(writers...)
	if settings.Application != nil {
		log.Logger = zerolog.New(mw).With().
			Str("application", *settings.Application).
			Str("version", version).
			Str("env", os.Getenv("APP_ENV")).
			Timestamp().
			Logger()
	} else {
		log.Logger = zerolog.New(mw).With().
			Str("version", version).
			Str("env", os.Getenv("APP_ENV")).
			Timestamp().
			Logger()
	}

	return nil
}
