package zerologsettings

import (
	"context"
	zlg "github.com/mark-ignacio/zerolog-gcp"
	"io"
)

type GCOLSettings struct {
	ProjectID string `yaml:"projectId"`
}

func (gcol *GCOLSettings) getWriter(application string) (writer io.Writer, err error) {
	return zlg.NewCloudLoggingWriter(context.Background(), gcol.ProjectID, application, zlg.CloudLoggingOptions{})
}
