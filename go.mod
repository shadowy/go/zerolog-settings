module gitlab.com/shadowy/go/zerolog-settings

go 1.16

require (
	github.com/mark-ignacio/zerolog-gcp v0.5.0
	github.com/olivere/elastic/v7 v7.0.32
	github.com/rs/zerolog v1.31.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)
