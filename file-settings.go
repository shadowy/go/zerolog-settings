package zerologsettings

import (
	"gopkg.in/natefinch/lumberjack.v2"
	"io"
	"os"
	"path"
)

// FileSettings - settings for file.
type FileSettings struct {
	Directory  string `yaml:"directory"`
	Filename   string `yaml:"filename"`
	MaxSize    int    `yaml:"maxSize"`
	MaxBackups int    `yaml:"maxBackups"`
	MaxAge     int    `yaml:"maxAge"`
}

func (file *FileSettings) getWriter() (writer io.Writer, err error) {
	if err := os.MkdirAll(file.Directory, 0744); err != nil {
		return nil, err
	}
	return &lumberjack.Logger{
		Filename:   path.Join(file.Directory, file.Filename),
		MaxBackups: file.MaxBackups,
		MaxSize:    file.MaxSize,
		MaxAge:     file.MaxAge,
	}, nil
}
