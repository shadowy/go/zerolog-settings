package main

import (
	"errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/shadowy/go/zerolog-settings"
	"time"
)

func main() {
	application := "App"
	cfg := zerologsettings.Settings{
		Application: &application,
		Elastic: &zerologsettings.ElasticSettings{
			URL: "http://elastic.local:80",
		},
	}
	_ = cfg.Init()
	log.Logger.Debug().Str("id", "1111").Msg("test")
	log.Logger.Error().Err(errors.New("error text")).Str("id", "1111").Msg("test")
	log.Logger.Info().Str("id", "1111").Msg("test")
	time.Sleep(time.Second * 1)
}
