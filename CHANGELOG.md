# CHANGELOG

<!--- next entry here -->

## 0.3.0
2022-01-01

### Features

- add elasticsearch destination (4807c679a876bfa6144e72577e5d42123bd6ce5a)

### Fixes

- fix lint issues (a81bb1df98c94d610573a1a7a7924199facde9e7)

## 0.2.0
2021-08-16

### Features

- add support Google Cloud Operation Logging (65be1d6e5b024ed957c1cecf02df68a76452da88)

## 0.1.1
2021-04-27

### Fixes

- update output (375f50af5c6aa61fd928d9503f275ae110472156)

## 0.1.0
2021-04-27

### Features

- initial commit (74fab57f3a8b31deb1c17a95747df8966b131bdb)

## 1.1.1
2020-05-02

### Fixes

- fix issue with null value (1cd3404b449ed1654d8edce129ff7affd63d3343)

## 1.1.0
2020-04-29

### Features

- add user and password for elastic search (36aae38770f18a4cf0434084f9db2fb2cee1054d)

### Fixes

- update library and fix lint issues (59344a76ecfa3789651510bfc6d88bff83e9dcee)
- fix CHANGELOG.md (968ee9a9ea1bc45d49738a5f32186a44d38c852e)
- update CHANGELOG.md (9882c27c371b9a417e6201961e0ce1daff28faef)
- reset changes (3bb2e4d9c03dbf5cb793dbac14ce063b832012de)

